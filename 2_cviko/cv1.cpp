#include <iostream>

#define N 100
using namespace std;

/**
 * Nacita n hodnot do pola.
 * @param X pole celych cisel. Maximalny pocet prvkov je N=100
 * @param n realna velkost pola X
 * @return void
 * */
void nacitajPole(int X[N],int n);
/**
 * Vypise pole do jedneho riadku
 * @param X pole celych cisel. Maximalny pocet prvkov je N=100
 * @param n realna velkost pola X
 * @return void
 * */
void vypisPole(int X[N],int n);
/**
 * Hlada v poli X prvok z maximalnou hodnotou 
 * @param X pole celych cisel. Maximalny pocet prvkov je N=100
 * @param n realna velkost pola X
 * @return index maxilmalneho prvku.
 * */
int maxPole(int X[N],int n);
/**
 * Hlada v poli X prvok z minimalnou hodnotou 
 * @param X pole celych cisel. Maximalny pocet prvkov je N=100
 * @param n realna velkost pola X
 * @return index minimalnaho prvku.
 * */
int minPole(int X[N],int n);

/**
 * Vypocia priemernu hodnoty prvkov v poli.
 * @param X pole celych cisel. Maximalny pocet prvkov je N=100
 * @param n realna velkost pola X
 * @return aritmeticky priemer vsetkych platnych prvkov pola.
 * */
double priemerPole(int X[N],int n);

int main(){
	int A[N];
	int pocet;
	
	cout<<"Zadaj velkost pola: ";
	cin>>pocet;
	
	nacitajPole(A,pocet);
	vypisPole(A,pocet);
	
	int i_max, i_min; //index maximam a minima
	i_max=maxPole(A,pocet);
	i_min=minPole(A,pocet);
	double p = priemerPole(A,pocet);
	cout<<"Maximalny prvok="<<A[i_max]<<endl;
	cout<<"Minimalny prvok="<<A[i_min]<<endl;
	cout<<"Priemerna hodnota="<<p<<endl;
	
}



void nacitajPole(int X[N],int n){
	for(int i=0;i<n;i++){
		cin>>X[i];
	}
}

void vypisPole(int X[N],int n){
	for(int i=0;i<n;i++){
		cout<<X[i]<<" ";
	}
	cout<<endl;
}

int maxPole(int X[N],int n){
	int indexMax=0;
	// od indexu 0 netreba ist, pretoze na zaciatku predpokladame ze 
	// prvok s indexom 0 je ten maximalny.
	for(int i=1;i<n;i++){
		if(X[i]>X[indexMax]){
			indexMax = i;
		}
	}
	return indexMax;
}


int minPole(int X[N],int n){
	int indexMin=0;
	// od indexu 0 netreba ist, pretoze na zaciatku predpokladame ze 
	// prvok s indexom 0 je ten minimalny.
	for(int i=1;i<n;i++){
		if(X[i]<X[indexMin]){
			indexMin = i;
		}
	}
	return indexMin;
}

double priemerPole(int X[N],int n){
	double priemer=0;
	for(int i=1;i<n;i++){
		priemer+=X[i];
	}
	return (priemer/n);
}
