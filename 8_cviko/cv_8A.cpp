#include<iostream>
using namespace std;
/**
 * Datova struktura reprezentujuca uzol stromu.
 * Uzol ma dve casti: datovu a informacnu.
 * Datova cast:
 * 		double hodnota - reprezentuje ciselnu hodnotu uzla
 * 		char operand - reprezentuje matematicku operaciu,ktora sa bude robit medzi potomkami tohto uzla
 * Informacna cast:
 * 		lavy, pravy: ukazovatele na strukturu TUzol, teda potomkov aktualneho uzla
 * 
 * Poznamka k hodnote:
 * operand moze byt definovany: '+', '-', '*', '/'. Ak ma operand hodnotu '0', znamena to, ze tam nie je
 * ziaden operand a bude sa pracovat s ciselnou hodnotou uzla.
 */ 
struct TUzol{
	double hodnota;
	char operand;
	TUzol *lavy, *pravy;
};

/**
 * Vytvori novy TUzol stromu a naplni ho hodnotami.
 * Vo funkcii sa vytvori a alokuje sa pamatove miesto pre novy TUzol.
 * @param h ciselna hodnota uzla
 * @param o operand. Ak je pouzity operand '0', znamena to, ze operand sa ignoruje a pracuje sa s hodnotou
 * @param l ukazatel na existujuci uzol (lava vetva) stromu alebo NULL
 * @param p ukazatel na existujuci uzol (prava vetva) stromu alebo NULL
 * @retval ukazatel na novovytvoren7 uzol stromu
 * 
 * */
TUzol *vytvorUzol(double h, char o, TUzol *l,TUzol *p){
	TUzol *novy = new TUzol;
	novy->hodnota=h;
	novy->operand=o;
	novy->lavy = l;
	novy->pravy = p;
	return novy;
}

/**
 * Vypise strom.
 * Detaily su v prednaskach
 **/
void vypisInOrder(TUzol *strom){
	if(strom!=NULL){
		vypisInOrder(strom->lavy);
		if(strom->operand == '0'){
			cout<<strom->hodnota;
		}else{
			cout<<strom->operand;
		}
		vypisInOrder(strom->pravy);
	}
}

/**
 * Vypise strom.
 * Detaily su v prednaskach
 **/
void vypisPreOrder(TUzol *strom){
	if(strom!=NULL){
		if(strom->operand == '0'){
			cout<<strom->hodnota;
		}else{
			cout<<strom->operand;
		}
		vypisPreOrder(strom->lavy);
		vypisPreOrder(strom->pravy);
	}
}

/**
 * Vypise strom.
 * Detaily su v prednaskach
 **/
void vypisPostOrder(TUzol *strom){
	if(strom!=NULL){
		vypisPostOrder(strom->lavy);
		vypisPostOrder(strom->pravy);
		if(strom->operand == '0'){
			cout<<strom->hodnota;
		}else{
			cout<<strom->operand;
		}
	}
}

/**
 * Vypocita hodnotu stromu. Rekurzivny algoritmus.
 * Hodnota stromu sa vypocita nasledovne:
 * hodnota laveho podstromu  OPERAND hodnota praveho podstromu
 * kde OPERAND je +,-,*,/
 * Ak nema aktualny uzol potomkov, jeho hodnota sa zoberie z polozky hodnota.
 * @param strom korenovy uzol, ktory reprezentuje strom
 */
double vypocitaj(TUzol *strom){
	double vysledok;
	switch(strom->operand){
	case '0':
		vysledok=strom->hodnota;
		break;
	case '+':
		vysledok = vypocitaj(strom->lavy) + vypocitaj(strom->pravy);
		break;
	case '-':
		vysledok = vypocitaj(strom->lavy) - vypocitaj(strom->pravy);
		break;
	case '*':
		vysledok = vypocitaj(strom->lavy) * vypocitaj(strom->pravy);
		break;
	case '/':
		vysledok = vypocitaj(strom->lavy) / vypocitaj(strom->pravy);
		break;
	}
	return vysledok;
}
/**
 * --------------------DOMACA ULOHA------------------------------
 * Porovna lavu (strom->lavy) a pravu cast (strom->pravy)  stromu, resp lavy a pravy podstrom.
 * @param strom korenovy uzol, ktory reprezentuje strom
 * @retval  Ak su podstromy rovkaka, vrati 0.
 * ak ma lava cast podstromu vacsiu hodnotu ako prava cast, vrati 1
 * ak ma prava cast podstromu vacsiu hodnotu ako lava cast, vrati -1
 */
int porovnajStom(TUzol *strom){
	return 0;
}



int main(){
	
	/* vytvorenie stromu reprezentujuceho vyraz (a+b)*e */
	TUzol *a = vytvorUzol(2,'0',NULL,NULL);
	TUzol *b = vytvorUzol(4,'0',NULL,NULL);
	TUzol *c = vytvorUzol(0,'+', a , b);
	TUzol *e = vytvorUzol(8,'0',NULL,NULL);
	TUzol *d = vytvorUzol(0,'*', c , e);
	vypisInOrder(d);
	cout<<endl;
	vypisPreOrder(d);
	cout<<endl;
	vypisPostOrder(d);
	cout<<endl;
	cout<<vypocitaj(d);
	cout<<endl;
	
	TUzol *root=vytvorUzol(0,'+',
					vytvorUzol(0,'-',
						vytvorUzol(5,'0',NULL, NULL),
						vytvorUzol(0,'*',
							vytvorUzol(8,'0',NULL,NULL),
							vytvorUzol(4,'0',NULL,NULL)
						)
					),
					vytvorUzol(0,'/',
							vytvorUzol(12,'0',NULL,NULL),
							vytvorUzol(5,'0',NULL,NULL)
					)
				);
	cout<<vypocitaj(root);
	cout<<endl;

	system("pause");
	
}
