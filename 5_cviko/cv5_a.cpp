#include <iostream>

using namespace std;
#define N	500

void generuj_cisla(int pole[N]);


int hladaj_cislo(int pole[N], int cislo);
int hladaj_cislo_bin(int pole[N], int cislo);

int main(){
	int cisla[N];
	generuj_cisla(cisla);
	
	// v poli cisla budem hladat cislo x
	int x;
	cin>>x;
	
	//index hladaneho cisla x v poli cisla
	int index_x;
	index_x = hladaj_cislo_bin(cisla,x);
	if(index_x >= 0){
		cout<<"cislo "<<x<<" sa nachadza na pozicii "<<index_x<<endl;
		cout<<cisla[index_x];
	}else{
		cout<<"Cislo "<<x<<" nebolo najdene";
	} 
	//system("pause");
	return 0;
}


void generuj_cisla(int pole[N]){
	int i;
	for(i=0;i<N;i++){
		pole[i]=3*i;
	}
}

/**
 * Vyhlada prvok "cislo" v poli "pole". Je pouzit7 algoritmus linearneho vyhladavania.
 * Zlozitost algoritmu je O(N)
 * @param pole celociselne pole, v ktorm vyhladavam. rozmer pola je N
 * @param cislo cislo, ktore hladam v poli
 * @retval ak je hladanie spesne, vrati index pola "pole", na ktorom je hladany prvok. 
 *        V opacnom priapde vrati -1.
 */
int hladaj_cislo(int pole[N], int cislo){
	int index=-1;
	for(int i=0;i<N;i++){
		if(cislo == pole[i]){
			index=i;
		}
	}
	return index;
}
/**
 * Vyhlada prvok "cislo" v poli "pole". Je pouzit7 algoritmus binarneho vyhladavania.
 * Predpokladom tohto algoritmu je fakt, ze pole MUSI byt utriedene.
 * Zlozitost algoritmu je O(Log N)
 * @param pole celociselne pole, v ktorm vyhladavam. rozmer pola je N
 * @param cislo cislo, ktore hladam v poli
 * @retval ak je hladanie spesne, vrati index pola "pole", na ktorom je hladany prvok. 
 *        V opacnom priapde vrati -1.
 */
int hladaj_cislo_bin(int pole[N], int cislo){
	//indexy v poli
	// l - lavy prvok
	// p - pravy prvok
	// s - stredny prvok
	int l,p,s;
	int result=-1;
	l=0;
	p=N-1;
	while(l<=p){		/* TU BOLA CHYBA: porovnanie musi byt "<=" */
		// vypocet indexu stredneho prvku
		s= l + (p-l)/2;
		//testujem, ci sa cislo x zhoduje s prvokom pole[s]
		if(cislo == pole[s]){
			l=p;	// dalsia iteracia neprebehne, cyklus sa
					// skonci a "s" vysledok
			result = s;
		}
		//hladane cislo je vacsie ako prvok pola pole[s].
		//zmenim rozsah hladania: posuniem lavy index na stred
		// novy interval je teda (s+1, p)
		if(cislo > pole[s]){
			l = s + 1;
		}else{
			//hladane cislo je mensie ako prvok pola pole[s].
			//zmenim rozsah hladania: posuniem pravy index na stred
			// novy interval je teda (l, s-1)
			p = s - 1;
		}
	}
	return result;
}
