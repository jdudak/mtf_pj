#include <iostream>
#include <time.h>       /* time */


using namespace std;
#define N	500

typedef struct {
	int cit,men;
} TZlomok;

void vypis_zlomok(TZlomok z);
void generuj_zlomky(TZlomok pole[N]);
int hladaj_zlomok(TZlomok pole[N], TZlomok cislo);
int hladaj_zlomok_bin(TZlomok pole[N], TZlomok cislo);
double hodnota_zlomku(TZlomok a);



int main(){
	TZlomok zlomky[N];
	generuj_zlomky(zlomky);
	TZlomok z;
	
	cout<<"zadaj citatel a menovatel zlomku z:";
	cin>>z.cit>>z.men;
	
	//index hladaneho cisla x v poli cisla
	int index_x;
	index_x = hladaj_zlomok_bin(zlomky,z);
	if(index_x >= 0){
		cout<<"zlomok ";
		vypis_zlomok(z);
		cout<< " sa nachadza na pozicii "<<index_x<<endl;
	}else{
		cout<<"Cislo ";
		vypis_zlomok(z);
		cout<<" nebolo najdene";
	} 
	return 0;
}


void generuj_zlomky(TZlomok pole[N]){
	int i;
	for(i=0;i<N;i++){
		pole[i].cit=5*i;
		pole[i].men = (2*i+1);
	}
}

void vypis_zlomok(TZlomok z){
	cout<<z.cit<<"/"<<z.men<<endl;
}

/**
 * Vrati hodnotu zlomku cit/men ako realne cislo
 * @param a zlomok, z ktore pocitam hodnota
 * @retval hodnota zlonku je podiel citatela a menovatela.
 */
double hodnota_zlomku(TZlomok a){
	return (double)a.cit/a.men;
}

/**
 * Porovna 2 zlomky
 * @param a 1 zlomok
 * @param b 2 zlomok
 * @retval 1: ak plati a>b, 0: ak plati a==b, -1: ak plati a<b
 */
int porovnaj_zlomky(TZlomok a, TZlomok b){
	double ha=hodnota_zlomku(a);
	double hb=hodnota_zlomku(b);
	int result;
	if(ha==hb){
		result = 0;
	}else{
		if(ha>hb){
			result = 1;
		}else{
			result = -1;
		}
	}
	return result;
}

/**
 * Vyhlada zlomok z v poli "pole". Je pouzity algoritmus linearneho vyhladavania.
 * Zlozitost algoritmu je O(N)
 * @param pole - pole zlomkov (TZlomok), v ktorm vyhladavam. rozmer pola je N
 * @param z zlomok, ktore hladam v poli
 * @retval ak je hladanie uspesne, vrati index pola "pole", na ktorom je hladany prvok. 
 *        V opacnom priapde vrati -1.
 */
int hladaj_zlomok(TZlomok pole[N], TZlomok z){
	int index=-1;
	for(int i=0;i<N;i++){
		if(...){						/*	TODO: pouzi funckiu 	porovnaj_zlomky 	*/
			index=i;
		}
	}
	return index;
}


/**
 * Vyhlada zlomok z v poli "pole". Je pouzity algoritmus binarneho vyhladavania.
 * Predpokladom tohto algoritmu je fakt, ze pole MUSI byt utriedene.
 * Zlozitost algoritmu je O(Log N)
 * @param pole - pole zlomkov (TZlomok), v ktorm vyhladavam. rozmer pola je N
 * @param z zlomok, ktore hladam v poli
 * @retval ak je hladanie uspesne, vrati index pola "pole", na ktorom je hladany prvok. 
 *        V opacnom priapde vrati -1.
 */
int hladaj_zlomok_bin(TZlomok pole[N], TZlomok x){
	//indexy v poli
	// l - lavy prvok
	// p - pravy prvok
	// s - stredny prvok
	int l,p,s;
	int result=-1;
	l=0;
	p=N-1;
	while(l<=p){
		// vypocet indexu stredneho prvku
		s= l + (p-l)/2;
		//testujem, ci sa zlomok x zhoduje s prvokom pole[s]
		if(...)								/*	TODO: pouzi funckiu 	porovnaj_zlomky 	*/
		{
			l=p;	// dalsia iteracia neprebehne, cyklus sa
					// skonci a "s" vysledok
			result = s;
		}
		//hladany zlomok x je vacsi ako prvok pola pole[s].
		//zmenim rozsah hladania: posuniem lavy index na stred
		// novy interval je teda (s+1, p)
		if(...)								/*	TODO: pouzi funckiu 	porovnaj_zlomky 	*/
		{
			l = s + 1;
		}else{
			//hladane cislo je mensie ako prvok pola pole[s].
			//zmenim rozsah hladania: posuniem pravy index na stred
			// novy interval je teda (l, s-1)
			p = s - 1;
		}
	}
	return result;
}
