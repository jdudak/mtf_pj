#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */


using namespace std;


void nacitajMaticu(int **X,int r, int s);
void generujMaticu(int **X,int r, int s);
void vypisMaticu(int **X,int r, int s);
int scitajMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z);
int odcitajMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z);
int nasobMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z);

int main(){
	int rA,sA;
	int **A;
	int rB,sB;
	int **B;
	int **C;
		
	
	/* inicializacia heneratora nahodnych cisel */
	srand (time(NULL));
	
	cout<<"Zadaj velkost matice A: "<<endl;
	cin>>rA>>sA;
	cout<<"Zadaj velkost matice B: "<<endl;
	cin>>rB>>sB;
	
	/* alokaca pamati pre meticu A */
	A = new int*[rA];
	for(int i=0;i<rA;i++){
		A[i] = new int[sA];
	}
	/* alokaca pamati pre meticu B */
	B = new int*[rB];
	for(int i=0;i<rB;i++){
		B[i] = new int[sB];
	}
	
	/* alokaca pamati pre meticu C */
	/* velkot matice C bude ( max(rA,rB), max(sA,sB)) */
	int rC = (rA>rB)?rA:rB;
	int sC = (sA>sB)?sA:sB;
	C = new int*[rC];
	for(int i=0;i<rC;i++){
		C[i] = new int[sC];
	}
	
	generujMaticu(A,rA,sA);
	generujMaticu(B,rB,sB);
	
	vypisMaticu(A,rA,sA);
	vypisMaticu(B,rB,sB);
	cout<<"================================"<<endl;
	
	int daSa;
	daSa = scitajMatice(A,rA,sA,B,rB,sB,C);
	if(daSa){
		cout<<endl<<"A + B"<<endl;
		vypisMaticu(C,rB,sB);
	}else{
		cout<<"Matice sa nedaju scitat"<<endl;
	}

	daSa = odcitajMatice(A,rA,sA,B,rB,sB,C);
	if(daSa){
		cout<<endl<<"A - B"<<endl;
		vypisMaticu(C,rB,sB);
	}else{
		cout<<"Matice sa nedaju scitat"<<endl;
	}
	
	daSa = nasobMatice(A,rA,sA,B,rB,sB,C);
	if(daSa){
		cout<<endl<<"A * B"<<endl;
		vypisMaticu(C,rA,sB);
	}else{
		cout<<"Matice sa nedaju nasobit"<<endl;
	}
	
	/* dealokacia pamati pre matice */
	for(int i=0;i<rC;i++){
		delete []C[i];
	}
	delete []C;
	
	for(int i=0;i<rB;i++){
		delete []B[i];
	}
	delete []B;
	
	for(int i=0;i<rA;i++){
		delete []A[i];
	}
	delete []A;
}


/**
 * Nacita maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void nacitajMaticu(int **X,int r, int s){
	cout<<"Nacitanie matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cin>>X[i][j];
		}
	}
}

/**
 * Vygeneruje maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void generujMaticu(int **X,int r, int s){

	cout<<"Nacitanie matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			X[i][j]=(rand()%10);
		}
	}
}

/**
 * Vypise maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void vypisMaticu(int **X,int r, int s){
	cout<<"Vypis matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cout<<X[i][j]<<"\t";
		}
		cout<<endl;
	}
}

/**
 * Scita 2 matice:  Z = X+Y
 * @param X prva matica velkosti r1*s1
 * @param r1 pocet riadkov matice X
 * @param s1 pocet stlpcov matice X
 * @param Y druha matica velkosti r2*s2
 * @param r2 pocet riadkov matice Y
 * @param s2 pocet stlpcov matice Y
 * @param X vysledna matica
 * @return void. Vysledok scitania je v matici Z
 * */
int scitajMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z){
	if((r1==r2) && (s1==s2)){
		for(int i=0;i<r1;i++){
			for(int j=0;j<s1;j++){
				Z[i][j]=X[i][j]+Y[i][j];
			}
		}
		return 1;
	}
	return 0;
}


/**
 * Scita 2 matice:  Z = X+Y
 * @param X prva matica velkosti r1*s1
 * @param r1 pocet riadkov matice X
 * @param s1 pocet stlpcov matice X
 * @param Y druha matica velkosti r2*s2
 * @param r2 pocet riadkov matice Y
 * @param s2 pocet stlpcov matice Y
 * @param X vysledna matica
 * @return void. Vysledok scitania je v matici Z
 * */
int odcitajMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z){
	if(r1==s1 && r2==s2){
		for(int i=0;i<r1;i++){
			for(int j=0;j<s1;j++){
				Z[i][j]=X[i][j]-Y[i][j];
			}
		}
		return 1;
	}
	return 0;
}

/**
 * Vynasobi 2 matice:  Z = X*Y
 * @param X prva matica velkosti r1*s1
 * @param r1 pocet riadkov matice X
 * @param s1 pocet stlpcov matice X
 * @param Y druha matica velkosti r2*s2
 * @param r2 pocet riadkov matice Y
 * @param s2 pocet stlpcov matice Y
 * @param X vysledna matica
 * @return void. Vysledok nasobenia je v matici Z
 * */
int nasobMatice(int **X,int r1, int s1, int **Y,int r2, int s2, int **Z){
	if(s1==r2){
		for(int i=0;i<r1;i++){
			for(int j=0;j<s2;j++){
				Z[i][j]=0;
				for(int k=0;k<s1;k++){
					Z[i][j] += X[i][k]*Y[k][j];
				}
			}
		}
		return 1;
	}
	return 0;
}
