#include <iostream>  
#include <fstream>    /* fstream */

using namespace std;


void nacitajMaticu(ifstream &vstup, int **X,int r, int s);
void vypisMaticu(int **X,int r, int s);

int main(){
	int row,col; // v subore je matica. Toto budu jej rozmery 
	ifstream vstupnySubor("testA.txt"); //pokus o otvorenie suboru
	int **MaticaA; 	// ulozi sa tu metica zo suboru testA.txt
	if(vstupnySubor.is_open()){
		vstupnySubor>>row>>col;
		cout<<"Nacitam maticu. Rozmer: "<<row<<"x"<<col<<endl;
		MaticaA = new int*[row];
		for(int i=0;i<row;i++){
			MaticaA[i] = new int[col];
		}
		nacitajMaticu(vstupnySubor, MaticaA, row, col);
		cout<<"Vypis matice s rozmermi "<<row<<"*"<<col<<endl;
		vypisMaticu(MaticaA, row, col);
		
		for(int i=0;i<row;i++){
			delete []MaticaA[i];
		}
		delete []MaticaA;
		vstupnySubor.close();
	}else{
		cout<<"Subor neexistuje";
	}
	return 0;
}



/**
 * Nacita maticu o velkosti r*s
 * @param vstup vstupny datovy prud. bude pouzity na nacitanie obsahu suboru.
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void nacitajMaticu(ifstream &vstup, int **X,int r, int s){
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			vstup>>X[i][j];
		}
	}
}

/**
 * Vypise maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void vypisMaticu(int **X,int r, int s){
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cout<<X[i][j]<<"\t";
		}
		cout<<endl;
	}
} 
