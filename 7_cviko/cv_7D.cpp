#include <iostream>

using namespace std;
#define N 20

/**
 * Rekurzivny algoritmus pre vypocet hodnoty faktorialu.
 * Pocita sa podla nasledujucich pravidiel:
 * n! = n*(n-1)!
 * 0! = 1
 * @param n hodnota z ktorej sa vypocita faktorial
 * @retval n!
 **/
long faktorial(int n){
        if (n==0){
                return 1;
        }
        // rekurzivne volanie
        // pozor, return tu musi byt, aby sa spravne vypocitala hodnota
        return n*faktorial(n-1);
}

/**
 * Rekurzivny algoritmus pre vypocet Fibonacciho postupnosti
 * Pocita sa podla nasledujucich pravidiel:
 * A) fib(n) = fib(n-1) + fib(n-2)
 * B) fib(1) = 1
 * C) fib(0) = 0
 * @param n hodnota z ktorej sa n-te Fibonacciho cislo
 * @retval hodnota n-teho Fibonacciho cisla
 **/
long fibonaci(int n){
		//pravidlo C)
        if (n==0){
                return 0;
        }
        //pravidlo C)
        if (n==1){
                return 1;
        }
        //pravidlo A)
        return fibonaci(n-1)+fibonaci(n-2);
}

/**
 * Pomocna funkcia pre algorumus binarneho vyhladvania pomocou rekurzie.
 * Vzdy budeme hladat v poli P ale len v tej casti, ktora je ohranicena
 * indexami left a right.
 * @param P pole v ktorom su ulozene cisla v ktorych vyhladavame
 * @param left index laveho prvku pola
 * @param right index praveho prvku pola
 * @param x hladane cislo
 * @retval index najdeneho cisla, ak sa naslo, alebo -1, ak sa nenaslo
 **/
int bin_helper(int P[N],int left,int right,int x){
		// STOP podmienka. Ak podmienka plati, hladane cislo x sa v poli
		// nenachadza
        if(left>right){
                return -1;
        }
        // vypocet stredneho prvku (medzi lavym a pravym prvkom)
        int middle = left + (right-left)/2;
		// test, ci prvok v strede je ten, co hladame
        if (x == P[middle]){
				//ak ano, vratime jeho index
                return middle;
        }
        // ak je hladany prvok vacsi ako prvok v strede P[middle]
        // opat aplikujeme rekurzivny algoritmus v ktorom vypocitame
        // novu hodnotu laveho prvku
        if (x > P[middle]){
                return bin_helper(P,middle+1,right,x);
        }
        else{
			// ak je hladany prvok mensi ako prvok v strede P[middle]
			// opat aplikujeme rekurzivny algoritmus v ktorom vypocitame
			// novu hodnotu praveho prvku
                return bin_helper(P,left,middle-1,x);
        }
}
/**
 * Spusta funckiu pre binarhne vyhladavanie pomocou rekurzie. 
 * @param P pole v ktorom su ulozene cisla v ktorych vyhladavame
 * @param z hladane cislo
 * @param dlzka pocet prvkov v poli
 * @retval index najdeneho cisla, ak sa naslo, alebo -1, ak sa nenaslo
 **/
int bin_search(int P[N],int z,int dlzka){
        return bin_helper(P,0,dlzka,z);
}

int main()
{
        int A[N]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
        int x=8;
        int index = bin_search(A,x,20);
        cout<<index<<endl;

        return 0;
}
