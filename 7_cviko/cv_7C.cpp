#include <iostream>  
#include <fstream>    /* fstream */

using namespace std;


void nacitajMaticu(ifstream &vstup, int **X,int r, int s);
void zapisMaticu(ofstream &vystup,int **X,int r, int s);

int main(){
	int row,col; // v subore je matica. Toto budu jej rozmery 
	ifstream vstupnySubor("testA.txt"); //pokus o otvorenie suboru
	int **MaticaA; 	// ulozi sa tu metica zo suboru testA.txt
	if(vstupnySubor.is_open()){
		vstupnySubor>>row>>col;
		cout<<"Nacitam maticu. Rozmer: "<<row<<"x"<<col<<endl;
		MaticaA = new int*[row];
		for(int i=0;i<row;i++){
			MaticaA[i] = new int[col];
		}
		nacitajMaticu(vstupnySubor, MaticaA, row, col);

		
		/*do suboru sa zapise invertovana matica
		 * format: prvy riadok rozmery matice
		 * nalseduje samotna matica
		 */
		ofstream vystupnySubor("testC.txt"); 
		if(vystupnySubor.is_open()){
			
			zapisMaticu(vystupnySubor, MaticaA, row, col);
			vystupnySubor.close();
		}
		
		for(int i=0;i<row;i++){
			delete []MaticaA[i];
		}
		delete []MaticaA;
		vstupnySubor.close();
	}else{
		cout<<"Subor neexistuje";
	}
	return 0;
}



/**
 * Nacita maticu o velkosti r*s
 * @param vstup vstupny datovy prud. bude pouzity na nacitanie obsahu suboru.
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void nacitajMaticu(ifstream &vstup, int **X,int r, int s){
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			vstup>>X[i][j];
		}
	}
}

/**
 * Zapise transponovanu maticu X do suboru
 * @param vystup vystupny datovy prud. bude pouzity na zapis hodnot do suboru
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void zapisMaticu(ofstream &vystup,int **X,int r, int s){
	vystup<<s<<" "<<r<<endl;
	for(int i=0;i<s;i++){
		for(int j=0;j<r;j++){
			vystup<<X[j][i]<<" ";
		}
		vystup<<endl;
	}
} 
