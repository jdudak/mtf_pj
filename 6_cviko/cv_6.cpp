#include <iostream>  
#include <fstream>    /* fstream */
#include <sstream>   /* istringstream */
#include <stdlib.h>     /* atoi */

using namespace std;


void zisti_rozmer_matice(string retazec, int *r, int *s);
void nacitajMaticu(ifstream &vstup, int **X,int r, int s);
void vypisMaticu(int **X,int r, int s);
void nacitajPole(ifstream &vstup, int *X,int r);
void vypisPole(int *X,int r);

int hladaj_cislo(int *X, int n, int x);

int main(){
	
	string riadok;		//pre ulozenie nacitaneho riadku zo suboru
	string datovaStruktura, rozmer; // hlavicka suboru ma 2 casti
	int row,col; // v subore je matica alebo pole. Toto su jej/jeho rozmery 
	ifstream vstupnySubor("testB.txt"); //pokus o otvorenie suboru
	int **MaticaA; 	// ulozi sa tu metica zo suboru testA.txt
	int *PoleB;		// ulozi sa tu pole zo suboru testB.txt
	if(vstupnySubor.is_open()){
		getline(vstupnySubor, riadok);	//nacitam prvy riadok zo suboru
		istringstream istr(riadok);  //vytvorim si pomocny prud pre jednoduchsie parsovanie casti
		istr>>datovaStruktura>>rozmer; // do pripravenych premenych ulozim obsah hlavicky subora
		if(datovaStruktura=="MATRIX"){
			zisti_rozmer_matice(rozmer, &row, &col);
			cout<<"Nacitam maticu. Rozmer: "<<row<<"x"<<col;
			MaticaA = new int*[row];
			for(int i=0;i<row;i++){
				MaticaA[i] = new int[col];
			}
			nacitajMaticu(vstupnySubor, MaticaA, row, col);
			vypisMaticu(MaticaA, row, col);
			
			for(int i=0;i<row;i++){
				delete []MaticaA[i];
			}
			delete []MaticaA;
		}
		if(datovaStruktura=="ARRAY"){
			/* kto to bude chciet, nech sa na cviku ozve.
			 * Ocakávam ale aspon nejakú uroven zaujmu, teda pokus o vyriešenie tejto casti.
			 * 
			 * V tejto casti je:
			 * - zistenie skutocneho rozmeru pola
			 * - nacitanie pola zo suboru
			 * - pouzitie algoritmu binarneho vyhladavania rekurzivnoou metodou
			 *   Uloha znie: otestujte funckaiu binarneho vyhľadavania tak
			 *   ze sa presvedcite, ze kazdy prvok z pola PoleB tento
			 *   algotmus najde.
			 *   Inymi slovami, hladam prvok PoleB[i], ci sa nachadza v PoliB
			 *   Vysledok musi byt 500 uspesnych hladani
			 *   v pole PoleB hladame prvok X
			 */
			row = atoi(rozmer.data());
			PoleB = new int[row];
			nacitajPole(vstupnySubor, PoleB,row);
			int uspech=0;
			for(int i=0;i<row;i++){
				col=hladaj_cislo(PoleB, row, PoleB[i]);
				if(col>=0){
					uspech++;
				}
			}
			cout<<"Pocet uspesnych hladani: "<<uspech;
			delete []PoleB;
		}
		
		vstupnySubor.close();
	}else{
		cout<<"Subor neexistuje";
	}
	return 0;
}


void zisti_rozmer_matice(string retazec, int *r, int *s){
	
	int n= retazec.length();
	int i;
	for(i = 0;i<n;i++){
		if(retazec[i]=='x'){
			break;
		}
	}
	string r2;
	r2=retazec.substr(0, i);
	*r = atoi(r2.data());
	
	r2=retazec.substr(i+1, retazec.size());
	*s = atoi(r2.data());
	
}


/**
 * Nacita maticu o velkosti r*s
 * @param vstup vstupny datovy prud. bude pouzity na nacitanie obsahu suboru.
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void nacitajMaticu(ifstream &vstup, int **X,int r, int s){
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			vstup>>X[i][j];
		}
	}
}

/**
 * Vypise maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void vypisMaticu(int **X,int r, int s){
	cout<<"Vypis matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cout<<X[i][j]<<"\t";
		}
		cout<<endl;
	}
} 
/**
 * Do pripraveneho pola (pole musi byt alokovane) nacita r poloziek zo suboru.
 * @param vstup vstupny datovy prud. Predstavuje otvoreny subor
 * @param X pole, do ktoreho budeme nacitavat hodnoty
 * @param r pocet prvkov v poli X 
 * @return void
 * 
 **/
void nacitajPole(ifstream &vstup, int *X,int r){
	for(int i=0;i<r;i++){
		vstup>>X[i];
	}
}

/**
 * Vypise obsah pola na konzolu (cout)
 * @param X polem ktore sa bude bypisovat
 * @param r rozmer pola
 * @retval void
 **/
void vypisPole(int *X,int r){
	cout<<"Vypis pola s rozmerom "<<r<<endl;
	for(int i=0;i<r;i++){
			cout<<X[i]<<",";
	}
}

/**
 * Pomocna funkcia rekurzivneho algoritmu binarneho vyhladavania.
 * V pole Z hlada cislo x, a to presne na pozicii v strede medzi l a r.
 * @param Z - pole, v ktorom sa hladaa prvok x
 * @param l lavy index rozsahu pola
 * @param r pravy index rozsahu pola
 * @param x hladany prvok
 * @return -1 ak sa prvok x nenachadza v strede medzi r a l. 
 * 			Alebo index prvku x v poli X. Tento index ma hodnotu stredu medzi r a l.
 **/
static int hladaj_cislo_helper(int *Z, int l, int r, int x){
	int stred=l+(r-l)/2;
	//cout<<l<<"\t"<<stred<<"\t"<<r<<"\t|"<<Z[stred]<<endl;
	if(l<=r){
		if(Z[stred]==x){
			return stred;
		}
		if(x>Z[stred]){
			return hladaj_cislo_helper(Z,stred+1,r, x);
		}else{
			return hladaj_cislo_helper(Z,l,stred-1, x);
		}
	}
	return -1;
}

/**
 * Binarne vyhladavanie.
 * @param X - pole, v ktorom sa hladaa prvok x
 * @param n velkost pola X
 * @param x hladany prvok
 **/
int hladaj_cislo(int *X, int n, int x){
	return hladaj_cislo_helper(X,0,n-1, x);
}
