# MTF_PJ

Príklady z cvičení Programovacie jazyky.

## Cvičenie 2. práca s poľom
- vytvorenie poľa celých čísel
- načítanie a výpis prvkom poľa
- hľadanie mimia, maxima, priemeru prvkov poľa

## Cvičenie 3. 2-rozmerné statické polia
- definícia matice rozmerov N * N
- funkcia na načítanie a výpis prvkov matice s rozmermi r * s
- sčítanie a odčítanie 2 matíc


## Cvičenie 4. 2-rozmerné dynamické polia
- definícia matice rozmerov r * s pomocou dynamickej alokácie pamäti
- násobenie 2 matíc

## Cvičenie 5. Vyhľadávanie
- Lineárne a binárne
-- v poli celých čísel
-- v poli štruktúr (TZlomok)

## Cvičenie 6. Práca s textovým súborom
- Načítanie obsahu súboru
- Jednoduché parsovanie obsahu

## Cvičenie 7. Práca s textovým súborom
- vzorové ukážky programov

## Cvičenie 8. Binárny strom
- tvorba stromu a prechádzanie stromom
- vyhodnocovanie aritmetických výrazov pomocou binárneho stromu

## Cvičenie 9. Binárny strom
- tvorba stromu s komplexnou dátovou štruktúrou
- tvorba usporiadaného stromu pomocou rekurzie

## Cvičenie 10. Štruktúry, polia, triedenie
- Algoritmus triedenia InsertSort
- Triedenie poľa štruktúr
