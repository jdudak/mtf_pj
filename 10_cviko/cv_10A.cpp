#include<iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;


void InsertSort(int *pole, int pocet){
    int i,j;
    // i - index prveho prvku v neutriedenej casti pola
    // j-1 - index posledneho prvku v usporiadanej casti pola
    // tmp - triedený prvok
    int tmp;
    for(i=1; i<pocet; i++) // cez pole prejdeme pocet krat
    {
        j=i; 							// 0..i-1 su uz zotriedene
        tmp = pole[j]; 					// tento prvok budeme zoradovat
        while( (j>0) && (pole[j-1]>tmp) ) // hladame, kde 	
        {
            pole[j] = pole[j-1];		// prvok vlozime
            j--;
        }
        pole[j]=tmp;  				// vlozime prvok na spravne miesto
    }
}

int main(){
	int cisla[500];
	srand((unsigned)time(NULL));
	int n=500;
    for(int i=0;i<n;i++){
        cisla[i]=rand()%1000;
    }
    InsertSort(cisla,n);
    cout<<"---------------ZOTRIEDENE-----------------"<<endl;
    for(int i=0;i<n;i++){
        cout<<cisla[i]<<" ";
    }
    //system("pause");
}
