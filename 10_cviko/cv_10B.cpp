#include<iostream>
#include<fstream>
using namespace std;
 
/**
 * @brief      Štruktúra reprezentujúca Auto, resp. jeho základné parametre
 */
struct TAuto{
    string vyrobca; // vyrobca
    string model; // model, 
    int vykon; 		//vykon   [kW], 
    double spotreba;	//spotreba [l/100km]
};


/**
 * @brief      Vypíše dostupné informácie o položkách štruktúry TAuto
 * @param      a  Smerník na existujúci uzol (TUzol)
 */
void vypisAuto(TAuto *a){  
    cout<<a->vyrobca<<", "<<a->model<<"\tvykon:"<<a->vykon<<"kW, spotreba: "<<a->spotreba<<" l/100km"<<endl;
}

/**
 * @brief      Vypíše pole štruktúr TAuto
 * @param      auta     Pole smerníkov na štruktúru TAuto
 * @param      n     veľkosť pola auta
 */
void vypisAuta(TAuto **auta, int n){
    for(int i=0;i<n;i++){
        vypisAuto(auta[i]);
    }
}

/**
 * @brief      Do strumu pridá uzol tak, aby bol strom usporiadaný.
 *  			Algoritmus:
 *  			- ak uzol nemá hodnotu (položka uzol->data je NULL), tak sa do uzla priradí hodnota 
 *  			- ak uzol má hodnotu:
 *  				- porovná sa hodnota vkladaného údaju (udaj) s hodnotou dátovej časti aktuálneho uzla (uzol->data) 
 *  				- ak je hodnota vkladaného údaju menšia ako hodnota aktuálneho uzla:
 *  					- nová hodnota sa vloží do ľavej časti / ľavého stromu  :  PridajUzol(uzol->lavy, udaj)
 *  				- ak je hodnota vkladaného údaju väčšia ako hodnota aktuálneho uzla:
 *  					- nová hodnota sa vloží do pravej časti / pravého stromu :  PridajUzol(uzol->pravy, udaj)
 *					- pred samotným zavolaním rekurzívneho vkladania sa vytvorí na danom mieste (lavy/pravy uzol) nový uzol
 * @param      uzol  koreň stromu
 * @param      udaj  vkladaný údaj
 */
int porovnajAuta(TAuto *a,TAuto *b){
    int p1 = a->vyrobca.compare(b->vyrobca);
    int p2;
    if(p1 == 0){
        p2 = a->model.compare(b->model);
        if(p2 == 0){   
            if( a->vykon == b->vykon){
                if( a->spotreba == b->spotreba){
                    return 0;
                }else{
                    if( a->spotreba > b->spotreba){
                        return 1;
                    }else{
                        return -1;
                    }
                }
            }else{
                if( a->vykon > b->vykon){
                    return 1;
                }else{
                    return -1;
                }
            }
        }else{
            return p2;
        }
    }else{
        return p1;
    }
   
    return 0;
}
/**
 *	Triedenie metodou INSERT SORT.
 * @param pole - pole  smernikov na TAuto
 * @param pocet - pocet prvkov v poli TAuto
 */
void InsertSort(TAuto **pole, int pocet){
    int i,j;
    // i - index prveho prvku v neutriedenej casti pola
    // j-1 - index posledneho prvku v usporiadanej casti pola
    // tmp - triedený prvok
    TAuto *tmp;
    for(i=1; i<pocet; i++) // cez pole prejdeme pocet krat
    {
        j=i;                            // 0..i-1 su uz zotriedene
        tmp = pole[j];                  // tento prvok budeme zoradovat
        while( (j>0) && porovnajAuta(pole[j-1],tmp)>0)  // hladame, kde   
        {
            pole[j] = pole[j-1];        // prvok vlozime
            j--;
        }
        pole[j]=tmp;                // vlozime prvok na spravne miesto
    }
}


int main(){
    

    /* A */
    TAuto *auto1=new TAuto;
    auto1->vyrobca="Skoda";
    auto1->model="forman";
    auto1->spotreba=9.87;
    auto1->vykon = 34;
    vypisAuto(auto1);
    delete auto1;
    
    /* B */
    TAuto **auta;
    auta=new TAuto*[30];
    int n=0;
    
    ifstream vstupnySubor("auta.txt"); //pokus o otvorenie suboru
    if(vstupnySubor.is_open()){
        while(!vstupnySubor.eof()){
            auta[n] = new TAuto;
            vstupnySubor>>auta[n]->vyrobca>>auta[n]->model>>auta[n]->vykon>>auta[n]->spotreba;
            vypisAuto(auta[n]);
            n++;
        }
        vstupnySubor.close();
    }
    InsertSort(auta,n);
    cout<<"-------------------------------------"<<endl;
    vypisAuta(auta,n);
    
    for(int i=0;i<n;i++){
        delete auta[i];
    }
    delete []auta;
    //system("pause");
   
}
