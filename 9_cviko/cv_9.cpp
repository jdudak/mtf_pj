#include<iostream>
using namespace std;
 
/**
 * @brief      Štruktúra reprezentujúca Auto, resp. jeho základné parametre
 */
struct TAuto{
    string vyrobca; // vyrobca
    string model; // model, 
    int vykon; 		//vykon   [kW], 
    double spotreba;	//spotreba [l/100km]
};

/**
 * Definícia štruktúry pre tvobu binárneho stromu.
*/
struct TUzol{
    TAuto *data;
    TUzol *lavy, *pravy;
};
 
/**
 * @brief      Funkcia vytvorí inštanciu štruktúry TAuto a vráti smerník na túto štruktúru
 *
 * @param  vyr   výrobca
 * @param  mod   model auta
 * @param  vyk   výkon v kW
 * @param  spo   Spotreba v l/100km
 *
 * @return     Vráti smerník na novovotvorenú štruktúru TAuto.
 */
TAuto *vytvorAuto(string vyr, string mod, int vyk, double spo){
    TAuto *a = new TAuto;
    a->vyrobca = vyr;
    a->model = mod;
    a->vykon = vyk;
    a->spotreba = spo;
    return a;
}
 
/**
 * @brief      Funkcia pre porovnanie obahu tvoch štruktúru typu TAuto
 *				Porovnávanie je nasledovné:
 *				1. ako prvé sa porovnáva výrobca, ak je výrobva rovnaký:
 *				2. porovná sa model. Ak je model roznaký porovná sa:
 *				3. výkon. Ak je výkon rovnaký porovná sa:
 *				4. spotreba.
 *				V ľubovoľnom kroku sa porovnávanie môže zastaviť, ak nie sú štruktúry rovnaké.
 *				Ak sú všetky položky rovnaké, vráti sa 0.
 * @param      a     Prvá štruktúra TAuto
 * @param      b     Druhá štruktúra TAuto
 *
 * @return      1: ak je hodnota a>b
 * 			    -1: ak  je hodnota a<b
 * 			    0: ak sú obe hodnoty rovnaké.
 */
int porovnajUzly(TAuto *a,TAuto *b){
    int p1 = a->vyrobca.compare(b->vyrobca);
    int p2;
    if(p1 == 0){
        p2 = a->model.compare(b->model);
        if(p2 == 0){   
            if( a->vykon == b->vykon){
                if( a->spotreba == b->spotreba){
                    return 0;
                }else{
                    if( a->spotreba > b->spotreba){
                        return 1;
                    }else{
                        return -1;
                    }
                }
            }else{
                if( a->vykon > b->vykon){
                    return 1;
                }else{
                    return -1;
                }
            }
        }else{
            return p2;
        }
    }else{
        return p1;
    }
   
    return 0;
}
 
/**
 * @brief      Do strumu pridá uzol tak, aby bol strom usporiadaný.
 *  			Algoritmus:
 *  			- ak uzol nemá hodnotu (položka uzol->data je NULL), tak sa do uzla priradí hodnota 
 *  			- ak uzol má hodnotu:
 *  				- porovná sa hodnota vkladaného údaju (udaj) s hodnotou dátovej časti aktuálneho uzla (uzol->data) 
 *  				- ak je hodnota vkladaného údaju menšia ako hodnota aktuálneho uzla:
 *  					- nová hodnota sa vloží do ľavej časti / ľavého stromu  :  PridajUzol(uzol->lavy, udaj)
 *  				- ak je hodnota vkladaného údaju väčšia ako hodnota aktuálneho uzla:
 *  					- nová hodnota sa vloží do pravej časti / pravého stromu :  PridajUzol(uzol->pravy, udaj)
 *					- pred samotným zavolaním rekurzívneho vkladania sa vytvorí na danom mieste (lavy/pravy uzol) nový uzol
 * @param      uzol  koreň stromu
 * @param      udaj  vkladaný údaj
 */
void PridajUzol(TUzol *uzol, TAuto *udaj){
    if(uzol->data == NULL){
        uzol->data = udaj;
        uzol->lavy = uzol->pravy = NULL;
        return;
    }
    /*
     * POZOR!!!!
     * Tu bola chyba. Povodny riadok porovnajUzly(uzol->data,udaj)
     * Tym padom tam bolo pacne porovnavanie a usporadovalo sa to opacne.
     */
    if( porovnajUzly(udaj, uzol->data) < 0 ){
        //vkladame uzol do lavej casti
        if( uzol->lavy == NULL){
            uzol->lavy = new TUzol;
            uzol->lavy->data = NULL;
        }
        PridajUzol(uzol->lavy, udaj);
    }else{
        //vkladame uzol do pravej casti
        if( uzol->pravy == NULL){
            uzol->pravy = new TUzol;
            uzol->pravy->data = NULL;
        }
        PridajUzol(uzol->pravy, udaj);
    }
}
 
/**
 * @brief      Vypíše dostupné informácie o položkách štruktúry TAuto
 *
 * @param      a     Smerník na existujúci uzol (TUzol)
 */
void vypisData(TAuto *a){  
    cout<<a->vyrobca<<", "<<a->model<<", vykon:"<<a->vykon<<"kW, "<<a->spotreba<<" l/100km"<<endl;
}

/**
 * @brief      Výpis usporiadaného stromu
 *
 * @param      strom  Koreň stromu
 */
void vypisInOrder(TUzol *strom){
    if(strom!=NULL){
        vypisInOrder(strom->lavy);
        vypisData(strom->data);
        vypisInOrder(strom->pravy);
    }
}
 
int main(){
   
    TUzol *strom=new TUzol;
    strom->data = NULL;
    PridajUzol(strom, vytvorAuto("Mazda","tri",120,7.4) );
    PridajUzol(strom, vytvorAuto("Mazda","dva",80,6.5) );
    PridajUzol(strom, vytvorAuto("Mazda","beta",80,6.5) );
    PridajUzol(strom, vytvorAuto("Skoda","Octavia",77,5.5) );
 
    vypisInOrder(strom);
    //system("pause");
   
}
