#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define N 100
using namespace std;


void nacitajMaticu(int X[N][N],int r, int s);
void generujMaticu(int X[N][N],int r, int s);
void vypisMaticu(int X[N][N],int r, int s);
int scitajMatice(int X[N][N],int r1, int s1, int Y[N][N],int r2, int s2, int Z[N][N]);
int odcitajMatice(int X[N][N],int r1, int s1, int Y[N][N],int r2, int s2, int Z[N][N]);


int main(){
	int rA,sA;
	int A[N][N];
	int rB,sB;
	int B[N][N];
	int C[N][N];
	
	/* inicializacia heneratora nahodnych cisel */
	srand (time(NULL));
	
	cout<<"Zadaj velkost matice A: "<<endl;
	cin>>rA>>sA;
	generujMaticu(A,rA,sA);
	cout<<"Zadaj velkost matice B: "<<endl;
	cin>>rB>>sB;
	generujMaticu(B,rB,sB);
		
	vypisMaticu(A,rA,sA);
	vypisMaticu(B,rB,sB);
	cout<<"================================"<<endl;
	
	int daSa;
	daSa = scitajMatice(A,rA,sA,B,rB,sB,C);
	if(daSa){
		cout<<endl<<"A + B"<<endl;
		vypisMaticu(C,rB,sB);
	}else{
		cout<<"Matice sa nedaju scitat"<<endl;
	}

	daSa = odcitajMatice(A,rA,sA,B,rB,sB,C);
	if(daSa){
		cout<<endl<<"A - B"<<endl;
		vypisMaticu(C,rB,sB);
	}else{
		cout<<"Matice sa nedaju scitat"<<endl;
	}
	
}


/**
 * Nacita maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void nacitajMaticu(int X[N][N],int r, int s){
	cout<<"Nacitanie matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cin>>X[i][j];
		}
	}
}

/**
 * Vygeneruje maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void generujMaticu(int X[N][N],int r, int s){
	cout<<"Nacitanie matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			X[i][j]=(rand()%100);
		}
	}
}

/**
 * Vypise maticu o velkosti r*s
 * @param X 2-rozmerne pole (matica) celych cisel. Maximalny pocet prvkov je N=100
 * @param r pocet riadkov matice X
 * @param s pocet stlpcov matice X
 * @return void
 * */
void vypisMaticu(int X[N][N],int r, int s){
	cout<<"Vypis matice s rozmermi "<<r<<"*"<<s<<endl;
	for(int i=0;i<r;i++){
		for(int j=0;j<s;j++){
			cout<<X[i][j]<<"\t";
		}
		cout<<endl;
	}
}

/**
 * Scita 2 matice:  Z = X+Y
 * @param X prva matica velkosti r1*s1
 * @param r1 pocet riadkov matice X
 * @param s1 pocet stlpcov matice X
 * @param Y druha matica velkosti r2*s2
 * @param r2 pocet riadkov matice Y
 * @param s2 pocet stlpcov matice Y
 * @param X vysledna matica
 * @return void. Vysledok scitania je v matici Z
 * */
int scitajMatice(int X[N][N],int r1, int s1, int Y[N][N],int r2, int s2, int Z[N][N]){
	if((r1==r2) && (s1==s2)){
		for(int i=0;i<r1;i++){
			for(int j=0;j<s1;j++){
				Z[i][j]=X[i][j]+Y[i][j];
			}
		}
		return 1;
	}
	return 0;
}


/**
 * Scita 2 matice:  Z = X+Y
 * @param X prva matica velkosti r1*s1
 * @param r1 pocet riadkov matice X
 * @param s1 pocet stlpcov matice X
 * @param Y druha matica velkosti r2*s2
 * @param r2 pocet riadkov matice Y
 * @param s2 pocet stlpcov matice Y
 * @param X vysledna matica
 * @return void. Vysledok scitania je v matici Z
 * */
int odcitajMatice(int X[N][N],int r1, int s1, int Y[N][N],int r2, int s2, int Z[N][N]){
	if(r1==s1 && r2==s2){
		for(int i=0;i<r1;i++){
			for(int j=0;j<s1;j++){
				Z[i][j]=X[i][j]-Y[i][j];
			}
		}
		return 1;
	}
	return 0;
}
